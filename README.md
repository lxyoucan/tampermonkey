# 简介
Tampermonkey 是一款免费的浏览器扩展和最为流行的用户脚本管理器，它适用于 Chrome, Microsoft Edge, Safari, Opera Next, 和 Firefox。

虽然有些受支持的浏览器拥有原生的用户脚本支持，但 Tampermonkey 将在您的用户脚本管理方面提供更多的便利。 它提供了诸如便捷脚本安装、自动更新检查、标签中的脚本运行状况速览、内置的编辑器等众多功能， 同时Tampermonkey还有可能正常运行原本并不兼容的脚本。

它可以很快的安装好，来试一试吧！
[https://www.tampermonkey.net/?ext=dhdg&locale=zh](https://www.tampermonkey.net/?ext=dhdg&locale=zh)

# 目录
| 标题                                                  | 图文      |视频|日期
|:-------------------------------------------------|:----------------:|:-----------:|:--------------------:
01去掉烦人的外链提醒| [图文](https://blog.csdn.net/lxyoucan/article/details/121267702)  | [视频](https://www.bilibili.com/video/BV1zq4y137e2/)|2021-11-11
02我是如何优化B站评论区外链体验的|  [图文](https://blog.csdn.net/lxyoucan/article/details/121283251)| [视频](https://www.bilibili.com/video/BV1FF411h7nQ/)|2021-11-12
03赖人必备自动点赞插件开发|  [图文](https://blog.csdn.net/lxyoucan/article/details/121299666)| [视频](https://www.bilibili.com/video/BV1vv411M7Rj/)|2021-11-13
04无限三连之术(幻术)|  [图文](https://blog.csdn.net/lxyoucan/article/details/121313896)| [视频](https://www.bilibili.com/video/BV1Zr4y1r7o8)|2021-11-14

# 支持一下
觉得有帮助, 记得给（UP主｜博主）[买个煎饼](https://note.youdao.com/s/XKXIu2lB), 贫穷码农在线乞讨。
![在这里插入图片描述](https://img-blog.csdnimg.cn/9620d4f27a984b95be1e12abee642b1b.png)

提供油猴插件私人定制服务。如有需要，请把需求发到我的邮件lxyoucan@163.com
