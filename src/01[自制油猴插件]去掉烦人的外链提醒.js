// ==UserScript==
// @name         CSDN link
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       ITKEY
// @match        *://*.csdn.net/*
// @icon         https://www.google.com/s2/favicons?domain=csdn.net
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    //我们来把这个event去掉方式，因为csdn 中使用了jquery，我们直接使用jquery.off()方法
    $("#content_views").off();
    //我们想新的页面在标签页面打开，而不是直接在当前页面打开
    //加个target就能解决了
    $("#content_views a[href]").attr("target","_blank");
})();