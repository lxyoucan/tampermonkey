// ==UserScript==
// @name         bilibili max like
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       ITKEY
// @match        https://www.bilibili.com/video/*
// @require      https://cdn.bootcdn.net/ajax/libs/jquery/1.9.1/jquery.js
// @icon         http://static.hdslb.com/images/favicon.ico
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    //步长默认为1
    var step = 1;
    $(".tit").click(function(){
        var inputdata = window.prompt('请输入每次点击的步长，只能输入纯数字。比如：1，100，999等');
        step = parseInt(inputdata);
        //先去除原来的event
        //今天介绍一个野路子，摧毁它，并重新创建
        var $ops = $(".ops");
        $ops.html($ops.html());

        //创建自己的点击事件
        $(".like,.coin,.collect").click(function(){
            console.log('---一直点赞，一直爽----');
            //获取当前的点赞的值
            var flag = '点赞数';
            var nc = $(this).attr("class");
            if(nc.indexOf('coin')>=0){
                flag = '投硬币枚数';
            }else if(nc.indexOf('collect')>=0){
                flag = '收藏人数';
            }
            var title = $(this).attr('title');
            //替换字符串获取点击的数量
            var count = title.replace(flag,'');
            count = parseInt(count);
            //增加并修改title属性
            $(this).attr("title",flag +(count+step));
            //鼠标放上去，小提示已经变了，我们还需要把显示的也变一下
            var oldStr = $(this).html();
            //用于展示的内容
            var showStr = parseInt(count)+parseInt(step);
            //数字过万，就显示的不正常了，需要处理一下
            if(showStr >=10000){
                //小数点保留一位就行了
                showStr = ((count+step)/10000).toFixed(1) + '万';
            }
            var newStr = oldStr.substring(0,oldStr.indexOf('</i>')+4)+showStr;
            //把新的html设置到元素上去
            $(this).html(newStr);
        })
})


//好了，目前我们的程序正常运行了，但是我想我们这脚本可以设置点赞步长，用户diy
//下面我们把脚本放到油猴里吧，在测试一下看看效果
})();
