// ==UserScript==
// @name         bilibili auto like
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       ITKEY
// @match        https://www.bilibili.com/video/*
// @icon         http://static.hdslb.com/images/favicon.ico
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    //下面我们来设置一下点击逻辑，如果在视频上停留超过N秒后就自动点赞
    //脚本测试正常，没想到一次就成功了，感谢您的观看。886

    //定义一个变量，保存配置n,当时间大于这个值我们就自动点赞👍

    var autoLikeWatchSecond = 5;//单位是秒 ，这个时间为了测试我们设置的比较短，实际上可以调整比如50秒，500秒之类的

    //定义变量保存上次的标题信息，用于对比
    var lastTitle = "";
    //保存视频开启播放的时间戳
    var playTime = new Date().getTime();
    //防止重复点赞，我们要把是否点赞记录下来
    var isLiked = false;
    //先写个定时器，用于检查当前播放的视频标题
    var timer = setInterval(function(){
        var titleObj = document.getElementsByClassName('tit');
        if(titleObj&&titleObj.length>0){
            var title = titleObj[0].innerHTML;
            console.log(title);
            //判断当前是否已经换了新的视频
            if(lastTitle!=title){
                console.log('----现在播放的是新的视频---');
                //修改变量
                lastTitle = title;
                //新视频要修改开始时间
                playTime = new Date().getTime();
                isLiked = false;
            }else{
                console.log('---主人正在观看视频---');
                //我们来计算一下视频播放的大概时间
                //当前的时候-刚开始播放的时间，得到时长
                var s = (new Date().getTime()- playTime)/1000;
                console.log('---已经播放了---'+s+'秒');
                //判断播放时长如果大于设置的值就自动给播放的视频点赞
                if(s>autoLikeWatchSecond&&isLiked==false){
                    //点赞
                    //我们先来实现脚本点赞👍
                    var like = document.getElementsByClassName('like');
                    //触发点击事件
                    like[0].click();
                    //我们设置的5秒后点赞，已经成功了。
                    console.log('-----已经为主人自动点赞-----');
                    //下面我们来切换视频测试一下。
                    //很简单几行代码搞定
                    isLiked = true;
                }

            }
        }
        //把定时器的时间间隔设置成3秒,每3秒获取一次标题
        //切换新的视频也能获取到新的标题
    },3000);

})();
