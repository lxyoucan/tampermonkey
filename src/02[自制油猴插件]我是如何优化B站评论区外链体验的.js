// ==UserScript==
// @name         Bilibili link
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       ITKEY
// @match        *://*.bilibili.com/*
// @require      https://cdn.bootcdn.net/ajax/libs/jquery/1.9.1/jquery.js
// @icon         https://www.google.com/s2/favicons?domain=bilibili.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    //没有反应，那是因为评价是ajax加载的，异步加载，评价还没出来的时间我们的代码获取不到text节点。
    $("#app").mouseover(function(){

        //当用于鼠标在页面上移动时触发我们的代码
        console.log('--------鼠标移动啦-------');
        //获取链接的节点
        //简介中的链接还不可以点击。 .desc-info
        //评论中的子评价链接还无法点击，我们来处理一下。 .text-con
        $(".text,.desc-info,.text-con").each(function(){
            var item = $(this).html();
            //出bug了，因为代码多次重复执行了，那么我们加个判断，已经增加的</a>就不在执行代码。
            if(item.indexOf('</a>')>=0){
                //console.log('----链接已存在！----');
                //可以啦，我们的小插件就开发完成了。
            }else{
                //那么我们要筛选出其中带的url的部分评论，这里用正则来筛选
                var result = item.match(/(https?|ftp|file):\/\/[-A-Za-z0-9+&@#\/%?=~_|!:,.;]+[-A-Za-z0-9+&@#/%=~_|]/g);
                //对匹配到的url处理，把它放进a标签中，使其成为一个超链接
                if(result && result.length>0){
                    for(var i=0;i<result.length;i++){
                        //循环输出结果
                        var urlStr = result[i];
                        //对这个url进行拼接字符串,让其从新标签页打开
                        var newLink = '<a href="'+urlStr+'" target="_blank">'+urlStr+'</a>';
                        //替换老的字符串为新的超链接字符串
                        item = item.replace(urlStr,newLink);
                    }
                }
                //把新的html源码放进节点之中
                $(this).html(item);
            }
        })
    });


})();
